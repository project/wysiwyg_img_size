<?php
/* @file
 * Contains \Drupal\wysiwyg_img_size\Form\settingsForm
 */
namespace Drupal\wysiwyg_img_size\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class settingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wysiwyg_img_size_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wysiwyg_img_size.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('wysiwyg_img_size.settings');

    // Get a list of entities in the content group.
    $entity_definitions = \Drupal::entityTypeManager()->getDefinitions();
    foreach ($entity_definitions as $definition) {
      if ($definition->getGroup() == 'content') {
        $entity_types[$definition->id()] = $definition->getLabel();
      }
    }

    $default = $config->get('entity_types');
    $form['entity_types'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity Types'),
      '#options' => $entity_types,
      '#default_value' => !empty($default) ? $default : array(),
      '#description' => $this->t('Select the entity types you would like to process.'),
    );

    $form['remote_username'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Remote Username'),
      '#default_value' => $config->get('remote_username'),
      '#description' => $this->t('If this site is password protected, enter the username needed to access the site.'),
    );
    $form['remote_password'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Remote Password'),
      '#default_value' => $config->get('remote_password'),
      '#description' => $this->t('If this site is password protected, enter the password needed to access the site.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('wysiwyg_img_size.settings');
    $config->set('entity_types', $form_state->getValue('entity_types'))
      ->set('remote_username', $form_state->getValue('remote_username'))
      ->set('remote_password', $form_state->getValue('remote_password'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
